# -*- coding: utf-8 -*-
{
    'name': "Perpustakaan ABC",

    'summary': """
        Sistem pengelolaan dan peminjaman buku perpustakaan.""",

    'description': """
        lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem.
    """,

    'sequence': 0,

    'author': "Muhammad Ikhsan Asa Pambayun",
    'website': "http://www.tba.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Perpustakaan',
    'application': True,
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'report_xlsx',
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/main.xml',
        'wizard/batal_peminjaman_views.xml',
        'views/buku_views.xml',
        'views/kategori_views.xml',
        'views/penulis_views.xml',
        'views/penerbit_views.xml',
        'views/rak_views.xml',
        'views/peminjam_views.xml',
        'views/peminjaman_views.xml',
        'report/report.xml',
        'report/report.xml',
        'report/data_peminjaman_pdf.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
