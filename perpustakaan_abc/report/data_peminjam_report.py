from odoo import models, fields


class DataPeminjamReport(models.AbstractModel):
    _name = 'report.data.peminjam'
    _inherit = 'report.report_xlsx.abstract'

    report_date = fields.Date.today()

    def generate_xlsx_report(self, workbook, data, peminjam):
        sheet = workbook.add_worksheet('Data Peminjam')
        sheet.write(0, 0, str(self.report_date))
        sheet.write(1, 0, 'Nama')
        sheet.write(1, 1, 'Alamat')
        sheet.write(1, 2, 'Umur')
        sheet.write(1, 3, 'Riwayat Peminjaman')
        row = 2
        for record in peminjam:
            col = 0
            sheet.write(row, col, record.name)
            sheet.write(row, col + 1, record.alamat)
            sheet.write(row, col + 2, record.umur)

            for item in record.peminjaman_ids:
                sheet.write(row, col + 3, item.buku_id.name)
                col += 1
            row += 1


class DataPeminjamanReport(models.AbstractModel):
    _name = 'report.data.peminjaman'
    _inherit = 'report.report_xlsx.abstract'

    report_date = fields.Date.today()

    def generate_xlsx_report(self, workbook, data, peminjaman):
        sheet = workbook.add_worksheet('Data Peminjaman')
        sheet.write(0, 0, str(self.report_date))
        sheet.write(1, 0, 'Id Peminjaman')
        sheet.write(1, 1, 'Buku')
        sheet.write(1, 2, 'Peminjam')
        sheet.write(1, 3, 'Tanggal Peminjaman')
        sheet.write(1, 4, 'Tanggal Pengembalian')
        sheet.write(1, 5, 'Status')
        row = 2
        for record in peminjaman:
            col = 0
            sheet.write(row, col, record.name)
            sheet.write(row, col + 1, record.buku_id.name)
            sheet.write(row, col + 2, record.peminjam_id.name)
            sheet.write(row, col + 3, str(record.tanggal_pinjam))
            sheet.write(row, col + 4, str(record.tanggal_pengembalian))
            sheet.write(row, col + 5, record.state)
            row += 1
