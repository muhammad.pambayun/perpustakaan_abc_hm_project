from odoo import api, fields, models


class Penerbit(models.Model):
    _name = 'perpustakaan.abc.penerbit'
    _description = 'Penerbit'
    _rec_name = 'name'

    name = fields.Char(
        string='Nama Penerbit',
        required=True,
    )

    buku_ids = fields.One2many(
        'perpustakaan.abc.buku',
        'penerbit_id',
        string='Buku',
    )

    _sql_constraints = [
        ('penerbit_name_unique', 'unique(name)', 'Data penerbit sudah ada.')
    ]
