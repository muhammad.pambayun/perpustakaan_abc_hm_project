from datetime import datetime
from odoo import api, fields, models


class Peminjaman(models.Model):
    _name = 'perpustakaan.abc.peminjaman'
    _description = 'Peminjaman'
    _rec_name = 'name'

    STATUS_CHOICES = [
        ('belum_dikembalikan', 'Belum dikembalikan'),
        ('sudah_dikembalikan', 'Sudah dikembalikan'),
        ('dibatalkan', 'Dibatalkan'),
    ]

    def _default_name(self):
        count = len(self.env['perpustakaan.abc.peminjaman'].search(
            []).mapped('id'))
        count += 1
        return 'P' + str(count)

    name = fields.Char(
        string='Id Peminjaman',
        required=True,
        default=_default_name
    )

    buku_id = fields.Many2one(
        'perpustakaan.abc.buku',
        string='Buku',
        required=True,
    )

    peminjam_id = fields.Many2one(
        'perpustakaan.abc.peminjam',
        string='Peminjam',
        required=True,
    )

    tanggal_pinjam = fields.Date(
        'Tanggal Peminjaman',
        default=datetime.today(),
        readonly=True,
    )

    tanggal_pengembalian = fields.Date(
        'Tanggal Pengembalian',
    )

    state = fields.Selection(
        STATUS_CHOICES,
        string='Status',
        required=True,
        default='belum_dikembalikan',
    )

    _sql_constraints = [
        ('peminjaman_name_unique', 'unique(name)', 'Data peminjaman sudah ada.')
    ]

    @api.model
    def create(self, values):
        buku = self.env['perpustakaan.abc.buku'].search([
            ('id', '=', values['buku_id'])
        ])[0]
        buku.dipinjam += 1
        return super(Peminjaman, self).create(values)

    @api.onchange('tanggal_pengembalian')
    def _onchange_state(self):
        if self.tanggal_pengembalian:
            self.state = 'sudah_dikembalikan'
            print(self.buku_id)
            buku = self.env['perpustakaan.abc.buku'].search([
                ('id', '=', self.buku_id.id)
            ])[0]
            buku.dipinjam -= 1
        else:
            self.state = 'belum_dikembalikan'
