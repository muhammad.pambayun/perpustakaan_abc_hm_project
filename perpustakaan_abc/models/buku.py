from email.policy import default
from odoo import api, fields, models
from odoo.exceptions import ValidationError


class Buku(models.Model):
    _name = 'perpustakaan.abc.buku'
    _description = 'Buku'
    _rec_name = 'name'

    name = fields.Char(
        string='Judul Buku',
        required=True,
    )

    penulis_ids = fields.Many2many(
        'perpustakaan.abc.penulis',
        string='Penulis',
    )

    penerbit_id = fields.Many2one(
        'perpustakaan.abc.penerbit',
        string='Penerbit',
        required=True,
    )

    penerbit = fields.Char(
        string='Nama Penerbit',
        default='',
        compute='_compute_penerbit',
        store=True,
    )

    tahun = fields.Char(
        string='Tahun Terbit',
        required=True,
    )

    kategori_id = fields.Many2one(
        'perpustakaan.abc.kategori',
        string='Kategori',
        required=True,
    )

    rak_id = fields.Many2one(
        related='kategori_id.rak_id',
    )

    jumlah = fields.Integer(
        string='Jumlah Total',
        required=True,
        default=0,
    )

    dipinjam = fields.Integer(
        string='Dipinjam',
        default=0,
    )

    status = fields.Selection(
        string='Status',
        selection=[('tersedia', 'Tersedia'), ('kosong', 'Kosong')],
        default='kosong',
        compute='_compute_status',
        store=True,
    )

    _sql_constraints = [
        ('buku_name_unique', 'unique(name)', 'Buku sudah ada.')
    ]

    @api.depends('penerbit_id')
    def _compute_penerbit(self):
        self.penerbit = self.penerbit_id.mapped('name')[0]

    @api.depends('dipinjam', 'jumlah')
    def _compute_status(self):
        if self.jumlah == 0:
            self.status = 'kosong'
        elif self.jumlah > self.dipinjam:
            self.status = 'tersedia'
        elif self.jumlah <= self.dipinjam:
            self.status = 'kosong'
        else:
            self.status = 'kosong'

    @api.constrains('dipinjam')
    def _check_dipinjam(self):
        for record in self:
            if record.dipinjam > record.jumlah:
                raise ValidationError(
                    'Buku dipinjam tidak boleh lebih dari jumlah buku yang ada.'
                )
