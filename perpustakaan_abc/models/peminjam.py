from odoo import api, fields, models


class Peminjam(models.Model):
    _name = 'perpustakaan.abc.peminjam'
    _description = 'Peminjam'
    _rec_name = 'name'

    name = fields.Char(
        string='Nama',
        required=True,
    )

    alamat = fields.Char(
        string='Alamat',
    )

    umur = fields.Char(
        string='Umur',
    )

    peminjaman_ids = fields.One2many(
        'perpustakaan.abc.peminjaman',
        'peminjam_id',
        string='Riwayat Peminjaman',
    )

    _sql_constraints = [
        ('peminjam_name_unique', 'unique(name)', 'Data peminjam sudah ada.')
    ]
