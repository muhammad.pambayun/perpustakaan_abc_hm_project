from odoo import api, fields, models


class Kategori(models.Model):
    _name = 'perpustakaan.abc.kategori'
    _description = 'Kategori'
    _rec_name = 'name'

    name = fields.Char(
        string='Nama',
        required=True,
    )

    rak_id = fields.Many2one(
        'perpustakaan.abc.rak',
        string='Rak',
        required=True,
    )

    buku_ids = fields.One2many(
        'perpustakaan.abc.buku',
        'kategori_id',
        string='Buku'
    )

    _sql_constraints = [
        ('kategori_name_unique', 'unique(name)', 'Kategori sudah ada.')
    ]
