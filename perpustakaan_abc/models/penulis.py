from odoo import api, fields, models


class Penulis(models.Model):
    _name = 'perpustakaan.abc.penulis'
    _description = 'Penulis'
    _rec_name = 'name'

    name = fields.Char(
        string='Nama',
        required=True,
    )

    buku_ids = fields.Many2many(
        'perpustakaan.abc.buku',
        string='Buku'
    )

    _sql_constraints = [
        ('penulis_name_unique', 'unique(name)', 'Data penulis sudah ada.')
    ]
