from email.policy import default
from odoo import api, fields, models
from odoo.exceptions import ValidationError


class Rak(models.Model):
    _name = 'perpustakaan.abc.rak'
    _description = 'Rak'
    _rec_name = 'name'

    def _default_name(self):
        count = len(self.env['perpustakaan.abc.rak'].search(
            []).mapped('id'))
        count += 1
        if count < 10:
            count = '000' + str(count)
        elif count < 100:
            count = '00' + str(count)
        elif count < 1000:
            count = '0' + str(count)
        else:
            str(count)
        return 'R' + str(count)

    name = fields.Char(
        string='Id Rak',
        required=True,
        default=_default_name,
    )

    kategori_ids = fields.One2many(
        'perpustakaan.abc.kategori',
        'rak_id',
        string='List Kategori',
    )

    lokasi = fields.Selection(
        [('1', 'Lantai 1'),
         ('2', 'Lantai 2'),
         ('3', 'Lantai 3')],
        string='Lokasi',
        required=True,
    )

    # kapasitas = fields.Integer(
    #     string='Kapasitas',
    #     required=True,
    # )

    # jumlah = fields.Integer(
    #     string='Jumlah Buku Disimpan',
    #     default=0,
    #     required=True,
    # )

    # status = fields.Char(
    #     string='Status',
    #     default='Tersedia',
    # )

    # @api.onchange('kapasitas', 'jumlah')
    # def _onchange_status(self):
    #     if self.kapasitas == 0:
    #         self.status = 'Penuh'
    #     elif self.kapasitas > self.jumlah:
    #         self.status = 'Tersedia'
    #     elif self.kapasitas <= self.jumlah:
    #         self.status = 'Penuh'
    #     else:
    #         self.status = 'Penuh'

    # @api.constrains('jumlah')
    # def _check_dipinjam(self):
    #     for record in self:
    #         if record.jumlah > record.kapasitas:
    #             raise ValidationError(
    #                 'Jumlah buku yang disimpan tidak dapat melebihi kapasitas rak.'
    #             )
