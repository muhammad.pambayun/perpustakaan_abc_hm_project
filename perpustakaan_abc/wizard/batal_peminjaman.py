from odoo import api, fields, models


class BatalPeminjaman(models.TransientModel):
    _name = 'batal.peminjaman.wizard'
    _description = 'Batal Peminjaman Wizard'

    def _default_peminjaman_id(self):
        id = self.env.context.get('active_id')
        if id:
            return self.env['perpustakaan.abc.peminjaman'].search([
                ('id', '=', id)
            ])[0]
        else:
            return False

    peminjaman_id = fields.Many2one(
        'perpustakaan.abc.peminjaman',
        string='Peminjaman',
        required=True,
        default=_default_peminjaman_id,
    )

    alasan = fields.Text(
        'Alasan',
        required=True,
    )

    def action_batal_peminjaman(self):
        self.peminjaman_id.state = 'dibatalkan'
        buku = self.env['perpustakaan.abc.buku'].search([
            ('id', '=', self.peminjaman_id.buku_id.id)
        ])[0]
        buku.dipinjam -= 1
